﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Settings
{
    /// <summary>
    /// Class with all settings set
    /// </summary>
    public class Settings : Settings_Functions
    {
        //*******************************************************************************************************************************************
        //  - Every value that is set in class bellow is default value.
        //  - Every value that you want to save in file must be public.
        //*******************************************************************************************************************************************

        //*******************************************************************************************************************************************
        //EXAMPLE

        private Server_Class server = new Server_Class();
        public Server_Class Server { set => server = value; get => server; }

        private User_Class user = new User_Class();
        public User_Class User { set => user = value; get => user; }

        //*****************************************************************************************
        public class Server_Class
        {
            private string ipAddress = "localhost";
            private ushort port = 1;

            public string IPAddress { set => ipAddress = value; get => ipAddress; }
            public ushort Port { set => port = value; get => port; }
        }

        //*****************************************************************************************
        public class User_Class
        {
            private string userId = "user";
            private bool useLogin = false;
            private string login = "user";
            private string password = "password";

            public string UserID { set => userId = value; get => userId; }
            public bool UseLogin { set => useLogin = value; get => useLogin; }
            public string Login { set => login = value; get => login; }
            public string Password { set => password = value; get => password; }
        }
    }
}
