﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace Settings
{
    /// <summary>
    /// Basic class that do everything functions with finnal class
    /// </summary>
    public abstract class Settings_Functions
    {
        /// <summary>
        /// Save current state of settings in xml type file
        /// </summary>
        /// <param name="path">Path where file will be save</param>
        /// <returns>True if no problems, false if somethink goes wrong - check file</returns>
        public bool SaveToXML(string path)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Settings));
                TextWriter writer = new StreamWriter(path);
                serializer.Serialize(writer, this);
                writer.Close();
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        //*******************************************************************************************************************************************
        /// <summary>
        /// Read settings from xml file
        /// </summary>
        /// <param name="path">Path where file is</param>
        /// <returns>Return settings if ok, otherwise null</returns>
        public Settings ReadFromXML(string path)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Settings));
                TextReader reader = new StreamReader(path);
                return (Settings)serializer.Deserialize(reader);
            }
            catch (Exception)
            {
                return null;
            }
        }

        //*******************************************************************************************************************************************
        /// <summary>
        /// Returns default settings set
        /// </summary>
        /// <returns>Defalt settings class</returns>
        public Settings GetDefault()
        {
            return new Settings();
        }
    }
}
