﻿namespace Settings_LibraryTest
{
    partial class MainForm
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.btSaveLocal = new System.Windows.Forms.Button();
            this.gbUser = new System.Windows.Forms.GroupBox();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.lbPassword = new System.Windows.Forms.Label();
            this.tbLogin = new System.Windows.Forms.TextBox();
            this.lbLogin = new System.Windows.Forms.Label();
            this.chbUseLogin = new System.Windows.Forms.CheckBox();
            this.tbUserId = new System.Windows.Forms.TextBox();
            this.lbUserId = new System.Windows.Forms.Label();
            this.gbServer = new System.Windows.Forms.GroupBox();
            this.lbServerPort = new System.Windows.Forms.Label();
            this.nudPort = new System.Windows.Forms.NumericUpDown();
            this.tbAddressIP = new System.Windows.Forms.TextBox();
            this.lbServerAddress = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tsmiSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSave = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRead = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSetDefault = new System.Windows.Forms.ToolStripMenuItem();
            this.gbUser.SuspendLayout();
            this.gbServer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPort)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btSaveLocal
            // 
            this.btSaveLocal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btSaveLocal.Location = new System.Drawing.Point(139, 251);
            this.btSaveLocal.Name = "btSaveLocal";
            this.btSaveLocal.Size = new System.Drawing.Size(114, 23);
            this.btSaveLocal.TabIndex = 8;
            this.btSaveLocal.Text = "SaveLocaly";
            this.btSaveLocal.UseVisualStyleBackColor = true;
            this.btSaveLocal.Click += new System.EventHandler(this.btSaveLocal_Click);
            // 
            // gbUser
            // 
            this.gbUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbUser.Controls.Add(this.tbPassword);
            this.gbUser.Controls.Add(this.lbPassword);
            this.gbUser.Controls.Add(this.tbLogin);
            this.gbUser.Controls.Add(this.lbLogin);
            this.gbUser.Controls.Add(this.chbUseLogin);
            this.gbUser.Controls.Add(this.tbUserId);
            this.gbUser.Controls.Add(this.lbUserId);
            this.gbUser.Location = new System.Drawing.Point(12, 107);
            this.gbUser.Name = "gbUser";
            this.gbUser.Size = new System.Drawing.Size(241, 130);
            this.gbUser.TabIndex = 7;
            this.gbUser.TabStop = false;
            this.gbUser.Text = "User";
            // 
            // tbPassword
            // 
            this.tbPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPassword.Location = new System.Drawing.Point(135, 94);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.PasswordChar = '*';
            this.tbPassword.Size = new System.Drawing.Size(100, 20);
            this.tbPassword.TabIndex = 6;
            this.tbPassword.Text = "password";
            this.tbPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbPassword
            // 
            this.lbPassword.AutoSize = true;
            this.lbPassword.Location = new System.Drawing.Point(6, 97);
            this.lbPassword.Name = "lbPassword";
            this.lbPassword.Size = new System.Drawing.Size(56, 13);
            this.lbPassword.TabIndex = 5;
            this.lbPassword.Text = "Password:";
            // 
            // tbLogin
            // 
            this.tbLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbLogin.Location = new System.Drawing.Point(135, 68);
            this.tbLogin.Name = "tbLogin";
            this.tbLogin.Size = new System.Drawing.Size(100, 20);
            this.tbLogin.TabIndex = 4;
            this.tbLogin.Text = "user";
            this.tbLogin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbLogin
            // 
            this.lbLogin.AutoSize = true;
            this.lbLogin.Location = new System.Drawing.Point(6, 71);
            this.lbLogin.Name = "lbLogin";
            this.lbLogin.Size = new System.Drawing.Size(36, 13);
            this.lbLogin.TabIndex = 3;
            this.lbLogin.Text = "Login:";
            // 
            // chbUseLogin
            // 
            this.chbUseLogin.AutoSize = true;
            this.chbUseLogin.Location = new System.Drawing.Point(9, 45);
            this.chbUseLogin.Name = "chbUseLogin";
            this.chbUseLogin.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chbUseLogin.Size = new System.Drawing.Size(74, 17);
            this.chbUseLogin.TabIndex = 2;
            this.chbUseLogin.Text = "Use Login";
            this.chbUseLogin.UseVisualStyleBackColor = true;
            // 
            // tbUserId
            // 
            this.tbUserId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbUserId.Location = new System.Drawing.Point(135, 19);
            this.tbUserId.MaxLength = 23;
            this.tbUserId.Name = "tbUserId";
            this.tbUserId.Size = new System.Drawing.Size(100, 20);
            this.tbUserId.TabIndex = 1;
            this.tbUserId.Text = "user";
            this.tbUserId.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbUserId
            // 
            this.lbUserId.AutoSize = true;
            this.lbUserId.Location = new System.Drawing.Point(6, 22);
            this.lbUserId.Name = "lbUserId";
            this.lbUserId.Size = new System.Drawing.Size(46, 13);
            this.lbUserId.TabIndex = 0;
            this.lbUserId.Text = "User ID:";
            // 
            // gbServer
            // 
            this.gbServer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbServer.Controls.Add(this.lbServerPort);
            this.gbServer.Controls.Add(this.nudPort);
            this.gbServer.Controls.Add(this.tbAddressIP);
            this.gbServer.Controls.Add(this.lbServerAddress);
            this.gbServer.Location = new System.Drawing.Point(12, 27);
            this.gbServer.Name = "gbServer";
            this.gbServer.Size = new System.Drawing.Size(241, 74);
            this.gbServer.TabIndex = 6;
            this.gbServer.TabStop = false;
            this.gbServer.Text = "Server";
            // 
            // lbServerPort
            // 
            this.lbServerPort.AutoSize = true;
            this.lbServerPort.Location = new System.Drawing.Point(6, 47);
            this.lbServerPort.Name = "lbServerPort";
            this.lbServerPort.Size = new System.Drawing.Size(29, 13);
            this.lbServerPort.TabIndex = 2;
            this.lbServerPort.Text = "Port:";
            // 
            // nudPort
            // 
            this.nudPort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.nudPort.Location = new System.Drawing.Point(135, 45);
            this.nudPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudPort.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudPort.Name = "nudPort";
            this.nudPort.Size = new System.Drawing.Size(100, 20);
            this.nudPort.TabIndex = 1;
            this.nudPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudPort.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tbAddressIP
            // 
            this.tbAddressIP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAddressIP.Location = new System.Drawing.Point(135, 19);
            this.tbAddressIP.Name = "tbAddressIP";
            this.tbAddressIP.Size = new System.Drawing.Size(100, 20);
            this.tbAddressIP.TabIndex = 0;
            this.tbAddressIP.Text = "localhost";
            this.tbAddressIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbServerAddress
            // 
            this.lbServerAddress.AutoSize = true;
            this.lbServerAddress.Location = new System.Drawing.Point(6, 22);
            this.lbServerAddress.Name = "lbServerAddress";
            this.lbServerAddress.Size = new System.Drawing.Size(61, 13);
            this.lbServerAddress.TabIndex = 0;
            this.lbServerAddress.Text = "IP Address:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiSettings});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(265, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tsmiSettings
            // 
            this.tsmiSettings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiSave,
            this.tsmiRead,
            this.tsmiSetDefault});
            this.tsmiSettings.Name = "tsmiSettings";
            this.tsmiSettings.Size = new System.Drawing.Size(61, 20);
            this.tsmiSettings.Text = "Settings";
            // 
            // tsmiSave
            // 
            this.tsmiSave.Name = "tsmiSave";
            this.tsmiSave.Size = new System.Drawing.Size(180, 22);
            this.tsmiSave.Text = "Save to file";
            this.tsmiSave.Click += new System.EventHandler(this.tsmiSave_Click);
            // 
            // tsmiRead
            // 
            this.tsmiRead.Name = "tsmiRead";
            this.tsmiRead.Size = new System.Drawing.Size(180, 22);
            this.tsmiRead.Text = "Open from file";
            this.tsmiRead.Click += new System.EventHandler(this.tsmiRead_Click);
            // 
            // tsmiSetDefault
            // 
            this.tsmiSetDefault.Name = "tsmiSetDefault";
            this.tsmiSetDefault.Size = new System.Drawing.Size(180, 22);
            this.tsmiSetDefault.Text = "Return defaults";
            this.tsmiSetDefault.Click += new System.EventHandler(this.tsmiSetDefault_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(265, 286);
            this.Controls.Add(this.btSaveLocal);
            this.Controls.Add(this.gbUser);
            this.Controls.Add(this.gbServer);
            this.Controls.Add(this.menuStrip1);
            this.Name = "MainForm";
            this.Text = "Settings - library test";
            this.gbUser.ResumeLayout(false);
            this.gbUser.PerformLayout();
            this.gbServer.ResumeLayout(false);
            this.gbServer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPort)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btSaveLocal;
        private System.Windows.Forms.GroupBox gbUser;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.Label lbPassword;
        private System.Windows.Forms.TextBox tbLogin;
        private System.Windows.Forms.Label lbLogin;
        private System.Windows.Forms.CheckBox chbUseLogin;
        private System.Windows.Forms.TextBox tbUserId;
        private System.Windows.Forms.Label lbUserId;
        private System.Windows.Forms.GroupBox gbServer;
        private System.Windows.Forms.Label lbServerPort;
        private System.Windows.Forms.NumericUpDown nudPort;
        private System.Windows.Forms.TextBox tbAddressIP;
        private System.Windows.Forms.Label lbServerAddress;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmiSettings;
        private System.Windows.Forms.ToolStripMenuItem tsmiSave;
        private System.Windows.Forms.ToolStripMenuItem tsmiRead;
        private System.Windows.Forms.ToolStripMenuItem tsmiSetDefault;
    }
}

