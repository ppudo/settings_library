﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Settings;

namespace Settings_LibraryTest
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            RefreshForm();
        }

        //*******************************************************************************************************************************************
        //VARIABLE

        /// <summary>
        /// Our current settings
        /// </summary>
        private Settings.Settings settings = new Settings.Settings();

        //*******************************************************************************************************************************************
        //FORM FUNCTIONS

        private void RefreshForm()
        {
            tbAddressIP.Text = settings.Server.IPAddress;
            nudPort.Value = (decimal)settings.Server.Port;

            tbUserId.Text = settings.User.UserID;
            chbUseLogin.Checked = settings.User.UseLogin;
            tbLogin.Text = settings.User.Login;
            tbPassword.Text = settings.User.Password;
        }

        //*******************************************************************************************************************************************
        //BUTTONS FUNCTIONS

        private void tsmiSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                settings.SaveToXML(dialog.FileName);
            }
        }

        //*****************************************************************************************
        private void tsmiRead_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                settings = settings.ReadFromXML(dialog.FileName);
                RefreshForm();
            }
        }

        //*****************************************************************************************
        private void tsmiSetDefault_Click(object sender, EventArgs e)
        {
            settings = settings.GetDefault();
            RefreshForm();
        }

        //*****************************************************************************************
        private void btSaveLocal_Click(object sender, EventArgs e)
        {
            settings.Server.IPAddress = tbAddressIP.Text;
            settings.Server.Port = (ushort)nudPort.Value;

            settings.User.UserID = tbUserId.Text;
            settings.User.UseLogin = chbUseLogin.Checked;
            settings.User.Login = tbLogin.Text;
            settings.User.Password = tbPassword.Text;
        }
    }
}
